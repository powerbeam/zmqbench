#!/usr/bin/python

import sys
import numpy as np
import matplotlib.pyplot as plt

def HashEntries(fname):
    d = {}
    with open(fname, 'r') as f:
        for line in f:
            parts = line.strip().split(' ')
            t = float(parts[0][1:-1])
            s = parts[1]
            a = parts[2]
            n = int(parts[3])

            if n not in d:
                d[n] = {}
            if a not in d[n]:
                d[n][a] = {}

            d[n][a][s] = t

    return d

if __name__ == "__main__":

    if len(sys.argv) != 3:
        print("two file names expected")
        exit()

    client = HashEntries(sys.argv[1])
    server = HashEntries(sys.argv[2])

    m = np.zeros([len(client), 4])
    i = 0
    for n in client:
        m[i,0] = client[n]['requested']['client']
        m[i,3] = client[n]['closed']['client']
        if n in server:
            m[i,1] = server[n]['received']['server']
            m[i,2] = server[n]['replied']['server']
        i += 1

    m = m - m[0][0]

    turnOver = m[:,3] - m[:,0]
    turnOver = turnOver[1:] * 1000

    plt.hist(turnOver)
    plt.show()
