#!/bin/bash

unbuffer ./Server > server.txt &
nc -l -p 1234 > client.txt
sleep 1
kill $(jobs -p)

./Stat.py client.txt server.txt
