# prerequisites

## Ubuntu

	sudo apt install libzmq5-dev expect
	pip install numpy matplotlib

# run like this

	cmake .
	make install
	cd install/bin
	./SelfTest.sh