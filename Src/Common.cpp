#include <time.h>
#include <stdio.h>

void PrintTime()
{
    struct timespec ts;
    clock_gettime(CLOCK_REALTIME, &ts);
    printf("%6lu.%09lu ", ts.tv_sec, ts.tv_nsec);
}
