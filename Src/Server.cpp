#include <string>
#include <stdio.h>
#include <stdint.h>

#include <zmq.hpp>

void PrintTime();

int main ()
{
    //  Prepare our context and socket
    zmq::context_t context(1);
    zmq::socket_t socket(context, ZMQ_REP);
    socket.bind ("tcp://*:5555");

    while (true) {
        zmq::message_t received;

        //  Wait for next request from client
        socket.recv(&received);
        PrintTime();
        printf("server received %u\n", *(uint32_t*)received.data());

        //  Send reply back to client
        zmq::message_t reply(1024);
        memcpy(reply.data(), received.data(), 1024);
        socket.send(reply);
        PrintTime();
        printf("server replied %u\n", *(uint32_t*)received.data());
    }
    return 0;
}
