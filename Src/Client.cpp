#include <string>
#include <stdio.h>
#include <stdint.h>

#include <zmq.hpp>

void PrintTime();

int main(int argc, char *argv[])
{
    //  Prepare our context and socket
    zmq::context_t context(1);
    zmq::socket_t socket(context, ZMQ_REQ);
    std::string addr = "localhost";
    if (argc >= 2) {
        addr = std::string(argv[1]);
    }
    addr = "tcp://" + addr + ":5555";
    socket.connect (addr);

    //  Do 10 requests, waiting each time for a response
    for (uint32_t request_nbr = 0; request_nbr != 3000; request_nbr++) {
        zmq::message_t request(1024);
        memcpy(request.data(), &request_nbr, 4);
        socket.send(request);
        PrintTime();
        printf("client requested %u\n", request_nbr);

        //  Get the reply.
        zmq::message_t reply;
        socket.recv(&reply);
        PrintTime();
        printf("client closed %u\n", *(uint32_t*)reply.data());
    }
    return 0;
}
